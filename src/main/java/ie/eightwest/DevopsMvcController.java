package ie.eightwest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Locale;

@Controller
public class DevopsMvcController {

    @RequestMapping(method= RequestMethod.GET, value="/home")
    public String home(Locale locale, Model model) {

        model.addAttribute("title", "test title");

        return "home";
    }
}
