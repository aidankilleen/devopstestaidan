package ie.eightwest;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTest {

    @Test
    public void seleniumTest() {

        System.setProperty("webdriver.chrome.driver",
                    "c:\\chromedriver\\chromedriver_win32\\chromedriver.exe");

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("headless");

        ChromeDriver driver = new ChromeDriver(chromeOptions);

        driver.get("http://168.63.36.18:8080/aidan");

        WebElement e = driver.findElementByTagName("h2");

        String title = e.getText();

        Assert.assertEquals("User Manager Web App", title);


        WebElement link = driver.findElementByLinkText("Home Page");

        link.click();

        title = driver.findElementByTagName("h1").getText();

        Assert.assertEquals("Home Page", title);

        driver.navigate().back();

        link = driver.findElementByLinkText("Static Page");

        link.click();

        title = driver.findElementByTagName("h1").getText();

        Assert.assertEquals("Static Page", title);

        WebElement image = driver.findElementByTagName("img");

        image.click();

        driver.switchTo().alert().accept();

        driver.navigate().back();

        WebDriverWait wait = new WebDriverWait(driver, 10);

        WebElement button = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.tagName("button")));

        Assert.assertNotNull(button);

        button.click();

        driver.switchTo().alert().accept();

        driver.quit();
    }
}
